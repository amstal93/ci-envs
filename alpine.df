#  alpine.df -- for a musl C library based build
#  Copyright © 2019, 2020  Olaf Meeuwissen
#
#  SPDX-License-Identifier: GPL-3.0-or-later

ARG   TAG
FROM  alpine${TAG} AS musl
LABEL maintainer="Olaf Meeuwissen <paddy-hack@member.fsf.org>"

ARG  RELEASE
RUN  test "$(sed -n 's/VERSION_ID=//p' /etc/os-release | cut -d. -f1-2)" \
          = ${RELEASE} \
 &&  apk add --no-cache \
         autoconf \
         automake \
         file \
         g++ \
         gettext \
         git \
         make \
         pkgconf \
         libtool \
         gcc \
         linux-headers \
         musl-dev \
         avahi-dev \
         curl-dev \
         libgphoto2-dev \
         libieee1284-dev \
         libjpeg-turbo-dev \
         libpng-dev \
         libxml2-dev \
         net-snmp-dev \
         poppler-dev \
         tiff-dev \
         libusb-dev \
         v4l-utils-dev
